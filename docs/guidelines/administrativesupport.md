
# Guidelines for Administrative Support

BEFORE THE VISIT:

**Information**: the Facility coordinator informs you about a new visit at the CERN facility with the following information: 
a.	Name of the users coming at CERN
b.	Exact dates of visit
c.	Email addresses
d.	Application form corresponding

**Eligibility**: Another verification of the eligibility of the users is needed. The eligibility criteria are the following: 

> - The user group leader and the majority of the users must work in a country other than the country(ies) where the installation is located.
>       - This does not apply where:
>           - Access is provided by an international organisation, the Joint Research Centre, an ERIC or similar legal entitie
>           - In case of remote access to a set of installations offering the same type of service and located in different countries
>       - EU financial support should not be offered to users from the same country where the installation is located. They are eligible to participate in the TNA project, but without EU financial support. 
> - Users groups comprised of a majority of users which do not work in an EU or associated country are eligible. However, their access is limited to 20% of the total amount of access units provided in Annex 1.

**Confirmation email**: The secretariat send a confirmation email to the User Group Leader with the following information (email template available at the CERN EU office) : 
    •	Financial support rules 
    •	Accommodation info
    •	Official documents to fill and sign to get financial support (letter of invitation, declaration on honour, proof of travel, confirmation of beamtime)
    •	Requirement to publish results
    •	Requirement to fill TA report

**Registration of TA Users**: If needed, the secretariat registers the TA users at CERN. The nature of registration depends on the secretariat and department involved. It can be external (EUPR) or CERN users (MPA). 

**Travel Tickets**:

*For externals:*
- If applicable, there are two options: 
    a.	External user book their own flight and the ticket can be reimbursed later in a Travel EDH document
    b.	The secretariat books and pays the flight through CWT.  
**For CERN users (MPA)**
- If applicable, there are two options: 
    a.	The CERN user books its own flight through CWT 
    b.	The secretariat books and pays the flight through CWT. 
> Tickets bought directly by the CERN users will not be reimbursed.

AFTER THE VISIT: 

**Reimbursement**
- For externals:

    - A Travel EDH document needs to be created to reimburse the travel tickets and Daily Travel Allowance. In the EDH document, the following rules need to be followed: 

| EDH Travel document:|
|---------------------|
| **Détails du voyage :** |
| Type de voyage : Invitation d'une personne non-membre du personnel <br /> Evènement: Add acronym of application form <br /> Code budgétaire: XXXXX <br /> Motif du voyage: Participation to the Transnational Access activity of the EU project EURO-LABS, GA no. 101057511, Project name: XXXXXXXX and TA Project Identifier: XXXXXXXXX <br /> Unité organique responsable du voyage: The secretariat in charge |
|**Itinéraire** |
|Add the travel information |
| **Frais** |
| Daily travel Allowance of 139CHF per day. <br /> Add Travel tickets expenses, if applicable. |
| **Attachments** |
| Letter of invitation signed <br /> Declaration on honour <br /> Proof of travel (boarding passes, tickets…) <br /> Travel ticket invoice, if applicable <br /> Confirmation of beamtime <br /> Bank account details if needed |

- For CERN users (MPA):
    - A Subsistence Allocation EDH document needs to be created to reimburse the subsistence of 139CHF per day. 
    - Attachments to the Edh document: 
        a.	Letter of invitation signed
        b.	Declaration on honour
        c.	Proof of travel (boarding passes, tickets…)
        d.	Confirmation of beamtime

